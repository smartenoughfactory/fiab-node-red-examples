# 'Factory in a Box' Example Flows

*These flows have prerequisite node requirements. You can [find out about the Factory in a box here](https://smartenoughfactory.atlassian.net/wiki/spaces/SEF/overview) for a full description of the project.*

---
## Flow Descriptions 

### testFlow.json
This is a flow to test out the apps making up the Factory in a Box (FIAB) system.

### backendFlow.json
This flow is a complete backend with MQTT pub/sub and database connection.

### dashboardFlow.json
This flow is an example dashboard for the FIAB system.

---
## Installation

Copy and paste the required flow (for example testFlow.json) contents from the clipboard to a running node-RED. The flow is preconfigured for the default docker services.


## Prerequisite nodes

1. node-red-contrib-xbee (installed) if [the default image](https://hub.docker.com/r/suttontools/fiab-nodered) is used.  Otherwise install it.  
2. node-red-contrib-re-postgres.
3. node-red-dashboard.

---
Enjoy.